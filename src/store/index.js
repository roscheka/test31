import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const API = 'http://am111.05.testing.place/api/v1/'
export default new Vuex.Store({
  state: {
    cars:[],
    car: {}
  },
  mutations: {
    SET_CARS_TO_STATE: (state, cars) => {
      state.cars = cars
    },
    SET_CAR_TO_STATE:(state,car) => {
      state.car = car
    }
  },
  actions: {
    GET_CARS_FROM_API({ commit }) {
      return axios.get(`${API}cars/list`)
      .then((res) => {
        return commit('SET_CARS_TO_STATE', res.data)
        })
        .catch((err) => {
        console.log(err)
        })
    },
    GET_CURRENT_CAR_FROM_API({commit},id){
      return axios.get(`${API}car/${id}`)
      .then((res) => {
        return commit('SET_CAR_TO_STATE', res.data)
        })
        .catch((err) => {
        console.log(err)
        })
    }
  },
  getters: {
    GET_CARS(state){
      return state.cars
    },
    GET_CAR(state){

      return state.car
    }
  }
})
